﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Scan_1._5.Views
{
    /// <summary>
    /// Логика взаимодействия для SetParametrsView.xaml
    /// </summary>
    public partial class SetParametrsView : Window
    {
        public SetParametrsView()
        {
            InitializeComponent();
            
        }

       
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator;
            var textBox = ((TextBox)sender);
            int caretIndex = textBox.CaretIndex;

            if (e.Text == "-")
            {
                if (caretIndex != 0)
                    e.Handled = true;
                return;
            }
            if (e.Text == "." || e.Text == ",")
            {
                e.Handled = true;

                if (textBox.Text.IndexOf(decimalSeparator, StringComparison.InvariantCulture) > 0 || caretIndex == 0)
                {
                    return;
                }
                textBox.Text = textBox.Text.Substring(0, caretIndex) + decimalSeparator + textBox.Text.Substring(caretIndex);
                textBox.CaretIndex = caretIndex + 1;
                return;
            }

            if (!Char.IsDigit(e.Text, 0))
                e.Handled = true;
        }
        
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            TextBoxA1.Focus();
            TextBoxA1.SelectAll();
        }

        private void TextBox_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            var textBox = (TextBox)sender;
            textBox.SelectAll();
        }

        private void TextBox_GotMouseCapture(object sender, MouseEventArgs e)
        {
            var textBox = (TextBox)sender;
            textBox.SelectAll();
        }


        private void TextBoxY1_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (Char.IsLetter(e.Text, 0) && e.Text!="x")
                e.Handled = true;
        }
    }
}
