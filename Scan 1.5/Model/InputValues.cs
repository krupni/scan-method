﻿using ELW.Library.Math.Expressions;

namespace Scan_1._5.Model
{
    class InputValues
    {
        /// <summary>
        /// Начальные значения интервалов
        /// </summary>
        public double A1 { get; set; }
        public double A2 { get; set; }
        public double A3 { get; set; }

        /// <summary>
        /// Конечные значения интервалов
        /// </summary>
        public double B1 { get; set; }
        public double B2 { get; set; }
        public double B3 { get; set; }

        /// <summary>
        /// Ограничения (интервалы первой функции)
        /// </summary>
        public double Y11 { get; set; }
        public double Y12 { get; set; }

        /// <summary>
        /// Ограничения (интервалы второй функции)
        /// </summary>
        public double Y21 { get; set; }
        public double Y22 { get; set; }

        /// <summary>
        /// Шаги
        /// </summary>
        public double H1 { get; set; }
        public double H2 { get; set; }
        public double H3 { get; set; }

        /// <summary>
        /// Цены
        /// </summary>
        public double C1 { get; set; }
        public double C2 { get; set; }
        public double C3 { get; set; }
        public double C4 { get; set; }


        public string Y1 { get; set; }
        public string Y2 { get; set; }

        public CompiledExpression F1 { get; set; }
        public CompiledExpression F2 { get; set; }
        /// <summary>
        /// "астрономическая" величина
        /// </summary>
        public double R { get; set; }

        public bool IsInitialized { get; set; }
    }
}
