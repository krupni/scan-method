﻿using System;

namespace Scan_1._5.Model
{
    class OutValues
    {
        public double X1 { get; set; }
        public double X2 { get; set; }
        public double X3 { get; set; }

        public double Y1 { get; set; }
        public double Y2 { get; set; }
        public double S { get; set; }

        public double X01 { get; set; }
        public double X02 { get; set; }
        public double X03 { get; set; }

        public double Y01 { get; set; }
        public double Y02 { get; set; }
        public double R { get; set; }

        public OutValues()
        {
            R = Math.Pow(10, 100);
        }
    }
}
