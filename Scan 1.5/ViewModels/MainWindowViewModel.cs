﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using System.Windows.Threading;
using ELW.Library.Math;
using ELW.Library.Math.Tools;
using Scan_1._5.Model;
using Scan_1._5.Views;
using Application = System.Windows.Forms.Application;

namespace Scan_1._5.ViewModels
{
    class StopException : Exception
    {

    }

    class MainWindowViewModel : INotifyPropertyChanged
    {

        public MainWindowViewModel(MainWindow view)
        {
            _view = view;
            _inValues = new InputValues();

            SetVisualElementsStatesToDefault();

            AboutCommand = new Command(arg=>About());
            SetParametrsValuesCommand = new Command(arg => SetParametrsValues());
            AutoStartCommand = new Command(arg => AutoStart());
            ManualStartCommand = new Command(arg => ManualStart());
            StopCommand = new Command(arg => Stop());
            ManualMoveCommand = new Command(arg => ManualMove());
            TimerInterval = 3;
            _timer = new DispatcherTimer();

            _timer.Tick += TimerTick;

            OutValues = new OutValues();
           
           
            HighlightBlock("1");

            //DEBUG ONLY
            //_inValues.IsInitialized = true;
            //OutValues.X1 = 100;
        }

        #region private fields

        private InputValues _inValues;
        private OutValues _outValues;
        private readonly MainWindow _view;
       
        /// <summary>
        /// Флаг автоматического режима управления
        /// </summary>
        private bool _isAuto;
        /// <summary>
        /// Флаг выполнения шага в ручном режиме
        /// </summary>
        private bool _isMove;
        #region Таймер - исопльзуется при автоматическом управлении

        private readonly DispatcherTimer _timer;
        private bool _stop;

        #endregion


        #endregion

        #region public fields

        public int TimerInterval { get; set; }

        public OutValues OutValues
        {
            get { return _outValues; }
            set { _outValues = value; OnPropertyChanged("OutValues"); }
        }

        public InputValues InValues
        {
            get { return _inValues; }
            set { _inValues = value; OnPropertyChanged("InValues"); }
        }
        #endregion



        #region Обработчики кнопок

        public Command AboutCommand { get; set; }

        private void About()
        {
            Views.About view = new About();
            view.Show();
        }
        /// <summary>
        /// кнопка "задать начальные значения"
        /// </summary>
        public Command SetParametrsValuesCommand { get; set; }

        private void SetParametrsValues()
        {
            var view = new SetParametrsView();
            var viewModel = new SetParametrsViewModel(view, _inValues);
            view.DataContext = viewModel;
            view.ShowDialog();
            _inValues = viewModel.InValues;
            OnPropertyChanged("InValues");
        }

        /// <summary>
        /// Автоматическое управление
        /// </summary>
        public Command AutoStartCommand { get; set; }

        private void AutoStart()
        {
            if (!_inValues.IsInitialized)
            {
                MessageBox.Show("Сначала следует задать начальные значения!");
                return;
            }
            SetVisualElementsStatesToExecutingMode(_view.ButtonAutoStop);
            _isAuto = true;
            MainFunction();

        }

        /// <summary>
        /// Ручное управление
        /// </summary>
        public Command ManualStartCommand { get; set; }

        private void ManualStart()
        {
            if (!_inValues.IsInitialized)
            {
                MessageBox.Show("Сначала следует задать начальные значения!");
                return;
            }
            SetVisualElementsStatesToExecutingMode(_view.ButtonManualStop, _view.ButtonManualMove);
            _isAuto = false;
            MainFunction();
        }

        public Command ManualMoveCommand { get; set; }

        private void ManualMove()
        {
            _isMove = true;
        }
        /// <summary>
        /// Остановка
        /// </summary>
        public Command StopCommand { get; set; }

        private void Stop()
        {
            throw new StopException();
        }

        #endregion

        /// <summary>
        /// Метод, который выполняет проход по блоксхеме
        /// </summary>
        private void MainFunction()
        {
            /* каждому блоку в блоксхеме соответствует метод, в котором производятся соответствующие этому блоку вычисления.
             * В MainFunction происходит последовательный вызов этих методов, кроме того, MainFunction отвечает за визуальное сопровождение процесса (подсвечивание блоков блоксхемы).
             * 
             * Почему используется goto.
             * Как известно, goto нерекомендуется использовать.
             * Однако, "в некоторых случаях он [goto] оказывается удобным и дает определенные преимущества, если  используется благоразумно." - Герберт Шилдт С# 4.0: полное руководство.
             * Имхо, в данном конкретном случае, использование goto правомерно, так как код писался в полном соответствии с блок-схемой, предоставленной в ТЗ. Конечно, я мог переделать 
             * код, заменив goto циклами, но это потребовало бы время на тестирование внесенных изменений. Времени на тестирование у меня не было, зато у меня было времия на то, что бы написать эту простыню текста.
             * 
             */
            OutValues = new OutValues();

            try
            {

                Block2Execute();
                HighlightBlock("2");
                Wait();

                M1:
                Block3Execute();
                HighlightBlock("3");
                Wait();

                M2:
                Block4Execute();
                HighlightBlock("4");
                Wait();

                M3:
                Block5Execute();
                HighlightBlock("5");
                Wait();

                bool fl = Block6Execute();
                HighlightBlock("6");
                Wait();
                if (!fl)
                    goto M4;

                Block7Execute();
                HighlightBlock("7");
                Wait();

                fl = Block8Execute();
                HighlightBlock("8");
                Wait();
                if (!fl)
                    goto M4;

                Block9Execute();
                HighlightBlock("9");
                Wait();

                fl = Block10Execute();
                HighlightBlock("10");
                Wait();
                if (!fl)
                    goto M4;

                BlockSaveExecute();
                HighlightBlock("Save");
                Wait();


                M4:
                Block11Execute();
                HighlightBlock("11");
                Wait();

                fl = Block12Execute();
                HighlightBlock("12");
                Wait();
                if (fl)
                    goto M3;

                Block13Execute();
                HighlightBlock("13");
                Wait();

                fl = Block14Execute();
                HighlightBlock("14");
                Wait();
                if (fl)
                    goto M2;

                Block15Execute();
                HighlightBlock("15");
                Wait();

                fl = Block16Execute();
                HighlightBlock("16");
                Wait();
                if (fl)
                    goto M1;

                HighlightBlock("17");

                while (true)
                {
                    Application.DoEvents();
                }
               
            }
            catch (StopException)
            {
                //Костыль
            }
            finally
            {
                SetVisualElementsStatesToDefault();
            }
        }

        private void Block2Execute()
        {
            OutValues.X3 = _inValues.A3;
            OnPropertyChanged("OutValues");
        }

        private void Block3Execute()
        {
            OutValues.X2 = _inValues.A2;
            OnPropertyChanged("OutValues");
        }

        private void Block4Execute()
        {
            OutValues.X1 = _inValues.A1;
            OnPropertyChanged("OutValues");
        }
        private void Block5Execute()
        {
            List<VariableValue> variables = new List<VariableValue>();
            variables.Add(new VariableValue(OutValues.X1, "x1"));
            variables.Add(new VariableValue(OutValues.X2, "x2"));
            variables.Add(new VariableValue(OutValues.X3, "x3"));

            OutValues.Y1 = ToolsHelper.Calculator.Calculate(_inValues.F1, variables);
            OnPropertyChanged("OutValues");
        }

        private bool Block6Execute()
        {
            return IsDoubleInRange(_inValues.Y11, OutValues.Y1, _inValues.Y21);
        }

        private void Block7Execute()
        {
            List<VariableValue> variables = new List<VariableValue>();
            variables.Add(new VariableValue(OutValues.X1, "x1"));
            variables.Add(new VariableValue(OutValues.X2, "x2"));
            variables.Add(new VariableValue(OutValues.X3, "x3"));

            OutValues.Y2 = ToolsHelper.Calculator.Calculate(_inValues.F2, variables);
            OnPropertyChanged("OutValues");
        }
        private bool Block8Execute()
        {
            if (IsDoubleInRange(_inValues.Y12, OutValues.Y2, _inValues.Y22))
                return true;
            return false;
        }
        private void Block9Execute()
        {
            OutValues.S = CalculateF0(_inValues.C1, _inValues.C2, _inValues.C3, _inValues.C4, OutValues.X1, OutValues.X2, OutValues.X3);
            OnPropertyChanged("OutValues");
        }

        private bool Block10Execute()
        {
            if (OutValues.S < OutValues.R)
                return true;
            return false;
        }
        private void BlockSaveExecute()
        {
            OutValues.X01 = OutValues.X1;
            OutValues.X02 = OutValues.X2;
            OutValues.X03 = OutValues.X3;

            OutValues.Y01 = OutValues.Y1;
            OutValues.Y02 = OutValues.Y2;
            OutValues.R = OutValues.S;
            OnPropertyChanged("OutValues");
        }
        private void Block11Execute()
        {
            OutValues.X1 = OutValues.X1 + _inValues.H1;
            OnPropertyChanged("OutValues");
        }
        private bool Block12Execute()
        {
            return IsDoubleInRange(_inValues.A1, OutValues.X1, _inValues.B1);
        }

        private void Block13Execute()
        {
            OutValues.X2 = OutValues.X2 + _inValues.H2;
            OnPropertyChanged("OutValues");
        }
        private bool Block14Execute()
        {
            return IsDoubleInRange(_inValues.A2, OutValues.X2, _inValues.B2);
        }

        private void Block15Execute()
        {
            OutValues.X3 = OutValues.X3 + _inValues.H3;
            OnPropertyChanged("OutValues");
        }
        private bool Block16Execute()
        {
            return IsDoubleInRange(_inValues.A3, OutValues.X3, _inValues.B3);
        }

        
        private double CalculateF0(double c1, double c2, double c3, double c4, double x1, double x2, double x3)
        {
            return c1 * x1 + c2 * x2 + c3 * c4 * (x3 - 14);
        }

        /// <summary> 
        /// Return true, if left &lt;=value&lt;=right
        /// </summary>
        private bool IsDoubleInRange(double left, double value, double right)
        {
            return (value >= left && value <= right);
        }
        private void SetVisualElementsStatesToDefault()
        {
            _view.ButtonAutoStart.Visibility = Visibility.Visible;
            _view.ButtonManualStart.Visibility = Visibility.Visible;
            _view.ButtonSetParametrsValues.Visibility = Visibility.Visible;
            _view.ButtonAutoStop.Visibility = _view.ButtonManualStop.Visibility = _view.ButtonManualMove.Visibility = Visibility.Hidden;

            _view.StackPanelLeft.Visibility = Visibility.Hidden;
            _view.StackPanelRight.Visibility = Visibility.Hidden;
            _view.TextBoxTimer.IsReadOnly = false;
            HighlightBlock("1");

        }


        private void SetVisualElementsStatesToExecutingMode(params Button[] buttons)
        {
            _view.ButtonAutoStart.Visibility =
                _view.ButtonAutoStop.Visibility =
                    _view.ButtonManualStart.Visibility =
                        _view.ButtonManualStop.Visibility = _view.ButtonSetParametrsValues.Visibility = Visibility.Hidden;

            foreach (var button in buttons)
            {
                button.Visibility = Visibility.Visible;
            }

            _view.TextBoxTimer.IsReadOnly = true;
            _view.StackPanelLeft.Visibility = Visibility.Visible;
            _view.StackPanelRight.Visibility = Visibility.Visible;

        }



        /// <summary>
        /// Ожидание нажатия кнопки "Шаг" при ручном управлении либо истечения промежутка времени (в автоматическом режиме)
        /// </summary>
        private void Wait()
        {
            if (!_isAuto)
            {
                while (!_isMove)
                {
                    Application.DoEvents();
                }
                _isMove = false;
                return;
            }
            else
            {

                _timer.Interval = new TimeSpan(0, 0, TimerInterval);
                _stop = true;
                _timer.Start();
                while (_stop)
                {
                    Application.DoEvents();
                }
            }

        }


        /// <summary>
        /// Подсвечивает блок в блок-схеме
        /// </summary>
        private void HighlightBlock(string blockName, int depth = 0, object obj = null)
        {
            if (obj == null)
            {
                obj = _view.Canvas;
            }
            // Иногда листовые узлы не принадлежат классу 
            // DependencyObject (например, строки)
            if (!(obj is DependencyObject))
                return;
            string nameToFind = "Block" + blockName;
            if (obj is Image)
            {
                Image image = (Image)obj;
                if (image.Name == nameToFind)
                    image.Effect = new BlurEffect();
                else
                {
                    image.Effect = null;
                }
            }
            foreach (object child in LogicalTreeHelper.GetChildren(obj as DependencyObject))
                HighlightBlock(blockName, depth + 1, child);

        }

        private void TimerTick(object sender, EventArgs e)
        {
            _timer.Stop();
            _stop = false;
        }


        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion

    }
}
