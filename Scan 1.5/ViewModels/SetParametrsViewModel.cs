﻿using System;
using System.ComponentModel;
using System.Windows;
using ELW.Library.Math;
using ELW.Library.Math.Expressions;
using Scan_1._5.Model;
using Scan_1._5.Views;

namespace Scan_1._5.ViewModels
{
    class SetParametrsViewModel : INotifyPropertyChanged
    {
        public SetParametrsViewModel(SetParametrsView view, InputValues inValues)
        {
            _view = view;
            InValues = inValues;
            SaveCommand = new Command(arg => Save());

#if DEBUG
            InValues.A1 = 50;
            InValues.A2 = 10;
            InValues.A3 = 50;

            InValues.B1 = 150;
            InValues.B2 = 30;
            InValues.B3 = 150;



            InValues.H1 = 50;
            InValues.H2 = 10;
            InValues.H3 = 50;

            InValues.Y11 = 0;
            InValues.Y21 = 300;
            InValues.Y12 = 0;
            InValues.Y22 = 300;

            InValues.Y1 = "x1+2*x2";
            InValues.Y2 = "x1+x2+x3";
            InValues.C1 = InValues.C2 = InValues.C3 = InValues.C4 = 10;
            InValues.R = Math.Pow(10, 100);
            InValues.IsInitialized = true;
#endif
        }

        private readonly SetParametrsView _view;

        public InputValues InValues { get; set; }
        public Command SaveCommand { get; set; }

        private void Save()
        {
            if (InValues.Y1 == null || InValues.Y2 == null)
            {
                MessageBox.Show("Сначала задайте Y1 и Y2.");
                return;
            }
            InValues.Y1 = InValues.Y1.Replace(",", ".");
            InValues.Y2 = InValues.Y2.Replace(",", ".");
            try
            {
                //http://habrahabr.ru/post/50158/
                PreparedExpression preparedExpression = ToolsHelper.Parser.Parse(InValues.Y1);
                CompiledExpression compiledExpression = ToolsHelper.Compiler.Compile(preparedExpression);
                // Optimizing an expression
                InValues.F1 = ToolsHelper.Optimizer.Optimize(compiledExpression);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка преобразования выражения  Y1.");
                return;
            }
            try
            {
                PreparedExpression preparedExpression = ToolsHelper.Parser.Parse(InValues.Y2);
                CompiledExpression compiledExpression = ToolsHelper.Compiler.Compile(preparedExpression);
                InValues.F2 = ToolsHelper.Optimizer.Optimize(compiledExpression);
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка преобразования выражения Y2.");
                return;
            }


            InValues.R = Math.Pow(10, 100);
            InValues.IsInitialized = true;
            _view.Close();
        }

        #region Implement INotyfyPropertyChanged members

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
