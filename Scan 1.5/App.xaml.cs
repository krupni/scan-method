﻿
using System.Windows;
using Scan_1._5.ViewModels;
using Scan_1._5.Views;

namespace Scan_1._5
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var view = new MainWindow();
            var viewModel = new MainWindowViewModel(view);
            view.DataContext = viewModel;
            view.Show();
        }
    }
}
